class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
  get name() {
    return this._name;
  }
  set name(value) {
    this._name = value;
  }
  get age() {
    return this._age;
  }
  set age(value) {
    this._age = value;
  }
  get salary() {
    return this._salary;
  }
  set salary(value) {
    this._salary = value;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  get lang() {
    return this._lang;
  }
  set lang(value) {
    this._lang = value;
  }
  get salary(){
    return super.salary * 3;
  }
  set salary(value) {
    super.salary = value;
  }
}

const programmerIvan = new Programmer("Ivan", 20, 1000, "english, ukraine");
const programmerFred = new Programmer("Fred", 25, 2000, "ukraine, german");
const programmerBoris = new Programmer("Boris", 30, 3000, "ukraine, english, german");

console.log(programmerIvan);
console.log(programmerFred);
console.log(programmerBoris);